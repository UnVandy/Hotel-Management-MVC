package Views;
import Controllers.OtherComponent;
import Controllers.ProductControllers;
import Models.DAO.ProductData;
import Models.DTO.Product;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

public class ProductView{

	//Add Product
	public Product addProductView() {
		Product sm=new Product();
		ProductControllers pc=new ProductControllers();
		int id = pc.getMaxID()+1;
		try {
			//get User input
			System.out.println("Product ID:" + id);
			String name = pc.inputString("Product Name=> ");
			int qty = pc.inputInteger("Quantity=> ");
			float price = pc.inputFloat("Unit Price=> ");

			//show to user
			System.out.println("Product ID:" + id);
			System.out.println("Produce Name:" + name);
			System.out.println("Quantity:" + qty);
			System.out.println("Unit Price:" + price);
			sm=new Product(id,name,qty,price);


		}catch(NumberFormatException e) {
			pc.redText("Input Invalid");
		}
		return sm;
	}

	//Read
	public void readDetail(Product pro) {
		Table t = new Table(1, BorderStyle.CLASSIC_COMPATIBLE, ShownBorders.SURROUND );
		//id=inputString("Please input ID or Name to Read: ");
		//t.setColumnWidth(0,25,40);
				t.addCell("  ID         : "+String.valueOf(pro.getId()));
				t.addCell("  Name       : "+pro.getName());
				t.addCell("  Quanity    : "+String.valueOf(pro.getQty()));
				t.addCell("  Unit Price : "+String.valueOf(pro.getPrice()));
				t.addCell("  Date import: "+pro.getDate());
				System.out.println(t.render());
	}
	//Update all
	public Product updateView(){
		Product update;
		ProductControllers pc=new ProductControllers();
		String name=pc.inputString("Please Input Name=>");
		int qty=pc.inputInteger("Please Input Quanity=>");
		float price=pc.inputInteger("Please Input Price=>");
		update=new Product(name,qty,price);
		return update;
	}
	public Product updateViewName(){
		Product update;
		ProductControllers pc=new ProductControllers();
		String name=pc.inputString("Please Input Name=>");
		update=new Product(name);
		return update;
	}
	public Product updateViewQty(){
		Product update;
		ProductControllers pc=new ProductControllers();
		int id=1;
		int qty=pc.inputInteger("Please Input Qty=>");
		update=new Product(id,qty);
		return update;
	}
	public Product updateViewprice(){
		Product update;
		int id=1;
		ProductControllers pc=new ProductControllers();
		float price=pc.inputFloat("Please Input Price=>");
		update=new Product(id,price);
		return update;
	}
	//Delete
	public int deleteView(){
		ProductControllers pc=new ProductControllers();
		return pc.inputInteger("Please input ID to Delete=>");
	}

	//Search
	public String searchView(){
		ProductControllers pc=new ProductControllers();
		return pc.inputString("Input Name to Seach=>");
	}

	 public void banner() {
			String ANSI_BLUE = "\u001B[34m";
			String ANSI_RESET = "\u001B[0m";
	        bannerRunning("                                    Welcome to                              ");
	        bannerRunning("                             Stock Management System                        ");
	        System.out.println("---------------------------------------------------------------------------");
	        bannerRunning(ANSI_BLUE+"   SSSSSSSSSSSSSSS  RRRRRRRRRRRRRRRRR                GGGGGGGGGGGGG   1111111 " );
	        bannerRunning(" SS:::::::::::::::S R::::::::::::::::R            GGG::::::::::::G  1::::::1   ");
	        bannerRunning("S:::::SSSSSS::::::S R::::::RRRRRR:::::R         GG:::::::::::::::G 1:::::::1   ");
	        bannerRunning("S:::::S     SSSSSSS RR:::::R     R:::::R       G:::::GGGGGGGG::::G 111:::::1   ");
	        bannerRunning("S:::::S              R::::R     R:::::R      G:::::G       GGGGGG    1::::1   ");
	        bannerRunning("S:::::S              R::::R     R:::::R     G:::::G                  1::::1   ");
	        bannerRunning(" S::::SSSS           R::::RRRRRR:::::R      G:::::G                  1::::1   ");
	        bannerRunning("  SS::::::SSSSS      R:::::::::::::RR       G:::::G    GGGGGGGGGG    1::::l   ");
	        bannerRunning("    SSS::::::::SS    R::::RRRRRR:::::R      G:::::G    G::::::::G    1::::l   ");
	        bannerRunning("       SSSSSS::::S   R::::R     R:::::R     G:::::G    GGGGG::::G    1::::l   ");
	        bannerRunning("            S:::::S  R::::R     R:::::R     G:::::G        G::::G    1::::l   ");
	        bannerRunning("            S:::::S  R::::R     R:::::R      G:::::G       G::::G    1::::l   ");
	        bannerRunning("SSSSSSS     S:::::S RR:::::R   R:::::::R       G:::::GGGGGGGG::::G 111::::::111");
	        bannerRunning("S::::::SSSSSS:::::S R::::::R   R:::::::R        GG:::::::::::::::G 1::::::::::1");
	        bannerRunning("S:::::::::::::::SS  R::::::R   R:::::::R          GGG::::::GGG:::G 1::::::::::1");
	        bannerRunning(" SSSSSSSSSSSSSSS    RRRRRRRR   RRRRRRRRR             GGGGGG   GGGG 111111111111"+ANSI_RESET);
	    	loading();
	    }
	 private void bannerRunning(String text) {
	        System.out.println( text);
	        try {
	            Thread.sleep(300);
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        }
	    }
	private void loading() {
		System.out.println();
		for (int i = 0; i < "Please wait...".length(); i++) {
			System.out.print("Please wait...".charAt(i));
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println();
	}
	public String menu() {
		String cmd;
		ProductControllers p=new ProductControllers();
        Table t = new Table(1, BorderStyle.CLASSIC, ShownBorders.SURROUND);
        t.addCell("  "+p.redText("*)")+"Display | "+p.redText("w)")+"rite | "+p.redText("R)")+"ead | " +
				p.redText("U)")+"pdate | "+p.redText("D)")+"elete | "+p.redText("F)")+"irst |" +
				p.redText("P)")+"revious | "+p.redText("N)")+"ext | "+p.redText("L)")+"ast    ");
        t.addCell("      "+p.redText("S)")+"earch | "+p.redText("G)")+"oto | " +
				p.redText("Se)")+"t row | "+p.redText("Sa)")+"ve | "+p.redText("Ba)")+"ck up | " +
				p.redText("Re)")+"store | "+p.redText("H)")+"elp | "+p.redText("E)")+"xit              ");
        System.out.println(t.render());
        //get user input and throw to controller
		cmd=p.inputString("Command:==> ").toLowerCase().trim();
		return cmd;
    }

	//Help
	public void help() {
		Table t = new Table(1, BorderStyle.CLASSIC_COMPATIBLE_WIDE, ShownBorders.SURROUND);

		t.addCell("1.press * : display all record of Products");
		t.addCell("2.press w : add new product");
		t.addCell(" press w->#proname-unit-price-qty : shortcut for add new product");
		t.addCell("3.press r : read content and content");
		t.addCell("press r#proId : sortcut for read product by Id");
		t.addCell("4.press u : update data");
		t.addCell("5.press d : delet data");
		t.addCell("press d#proId: shortcut for delete product by Id");
		t.addCell("6.press f : display first page ");
		t.addCell("7.press p : displau previos pages");
		t.addCell("8.press n : display nextpage");
		t.addCell("9.press l : display last page");
		t.addCell("10.press s : search product by name");
		t.addCell("11.press sa: save record to file");
		t.addCell("12.press ba: backup data");
		t.addCell("13.press re: restore data");
		System.out.print(t.render());
	}
	
    public void displayTable(int tableNo){
		CellStyle cellStyle=new CellStyle(CellStyle.HorizontalAlign.center);
		OtherComponent component = new OtherComponent();
		ProductData data = new ProductData();
		String ANSI_RED = "\u001B[31m";
		String ANSI_RESET = "\u001B[0m";
		int row=Integer.parseInt(component.readRow());
		int count=0;
		Table t1 = new Table(5, BorderStyle.CLASSIC_COMPATIBLE_WIDE, ShownBorders.ALL);
		t1.setColumnWidth(0, 10, 10);
		t1.setColumnWidth(1, 20, 25);
		t1.setColumnWidth(2, 20, 20);
		t1.setColumnWidth(3, 20, 20);
		t1.setColumnWidth(4, 17, 20);
		String header[] = {ANSI_RED+"ID", ANSI_RED+"Name", ANSI_RED+"Unit Price", ANSI_RED+"Quantity", ANSI_RED+"Date"+ANSI_RESET};
		t1.addCell(header[0],cellStyle);
		t1.addCell(header[1],cellStyle);
		t1.addCell(header[2],cellStyle);
		t1.addCell(header[3],cellStyle);
		t1.addCell(header[4],cellStyle);

		for(Product st:data.getProductData()){
			if(count>=(tableNo-1)*row&&count<tableNo*row) {
				t1.addCell(String.valueOf(st.getId()));
				t1.addCell(st.getName());
				t1.addCell(String.valueOf(st.getPrice()));
				t1.addCell(String.valueOf(st.getQty()));
				t1.addCell(st.getDate());
			}else if(count==tableNo*row) break;
			count++;
		}
		System.out.println(t1.render());
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		System.out.println(OtherComponent.sign("Page: ")+ OtherComponent.blueText(tableNo)+ OtherComponent.sign(" Of ")+ OtherComponent.blueText((int)Math.ceil(data.getProductData().size()/Double.valueOf(component.readRow())))+"\t\t\t\t\t\t\t\t\t\t\t"+ OtherComponent.sign("Total Records: ")+ OtherComponent.blueText(data.getProductData().size()));
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    }
}
