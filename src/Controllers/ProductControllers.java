package Controllers;

import Models.DAO.ProductData;
import Models.DTO.Product;
import Views.ProductView;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.Table;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class ProductControllers {
	//Write all logic and validate here
	private Product pro=new Product();
	private ProductView pv=new ProductView();
	private ProductData pd=new ProductData();

	//Add product 10M
	private void add10MProduct(){
		int num= inputInteger("Input number you want to add Product=>");
		String ch=inputString("Are you sure want to add "+num+ "records[Y/N]=>");
		if(ch.trim().equalsIgnoreCase("y")) {
			for (int i = 1; i <= num; i++) {
				pd.addProduct(new Product(i,"Coca", 10, 33));
			}
		}else {
			msg("Cancelled...");
		}
	}

	//Add product controller
	private void addProductControllers() {
		Product pro=pv.addProductView();
		String ch=inputString("Are you sure want to add this record? [Y/N]=>");
		if(ch.trim().equalsIgnoreCase("y")){
			pd.addProduct(pro);
			msg("Add Product Successfully");
		}else{
			msg("Cancelled...");
		}

	}

	//Read
	private void readControllers(int id){
		//int s=inputInteger("Input ID to read=> ");
		pro=pd.read(id);
		if(pro.getId()==0){
			msg("Not found for id "+id);
		}else {
			pv.readDetail(pro);
		}
	}

	//Update
	private void updateControllers(){
		int id= inputInteger("Please Input id=> ");
		int getId=pd.readConditon(id);
		if (getId==0){
			msg("ID Not Found....");
		}
		else {
			Table t=new Table(1,BorderStyle.CLASSIC);
			t.addCell("1) UpdateName 2) UpdateQty 3) UpdatePrice 4) UpdateAll ");
			System.out.println(t.render());
			do {
				int get= inputInteger("Please Choose Number to Update or press 100 to exit:");
				String ch;
				switch (get){
					case 1:
						pro=pv.updateViewName();
						pro=new Product(id,pro.getName());
						ch = inputString("Are you sure want to Update this name? [Y/N]=>");
						if (ch.trim().equalsIgnoreCase("y")) {
							pd.updateName(pro);
						} else {
							msg("Update Cancelled...");
						}
						break;
					case 2:
						pro=pv.updateViewQty();
						pro=new Product(id,pro.getQty());
						ch = inputString("Are you sure want to Update this qty? [Y/N]=>");
						if (ch.trim().equalsIgnoreCase("y")) {
							pd.updateQty(pro);
						} else {
							msg("Update Cancelled...");
						}
						break;
					case 3:
						pro=pv.updateViewprice();
						pro=new Product(id,pro.getPrice());
						ch = inputString("Are you sure want to Update this price? [Y/N]=>");
						if (ch.trim().equalsIgnoreCase("y")) {
							pd.updatePrice(pro);
						} else {
							msg("Update Cancelled...");
						}
						break;
					case 4:
						pro=pv.updateView();
						pro=new Product(id,pro.getName(),pro.getQty(),pro.getPrice());
						ch = inputString("Are you sure want to Update this record ? [Y/N]=>");
						if (ch.trim().equalsIgnoreCase("y")) {
							pd.update(pro);
						} else {
							msg("Update Cancelled...");
						}
						break;
					case 100:
						msg("You have been exit....");
						return;
					default:
						msg("Your input isn't correct please input 1=>4 ");
				}
			}while (true);

		}
	}

	//Delete
	private void deleteControllers(){
		int id=pv.deleteView();
		pro=pd.read(id);
		if(pro.getId()==0){
			msg("Not found for id "+id);
		}else {
			pv.readDetail(pro);
			String ch=inputString("Are you sure want to delete this record[Y/N]=>");
			if(ch.trim().equalsIgnoreCase("y")){
				pd.delete(id);
				msg("Delete successfully");
			}else {
				msg("Cancel...");
			}
		}

	}

	//Search
	private void searchControllers(){
		List<Product>list;
		String name=pv.searchView();
		list=pd.search(name);
		if(list.size()==0){
			msg("Search not found "+name);
		}else {
			pv.displayTable(1);
		}
	}

	//get Max ID
	public int getMaxID(){
		//System.out.println(pd.getLastID());
		return pd.getLastID();
	}
	//Input String only
	public String inputString(String text){
		Scanner in=new Scanner(System.in);
		System.out.print(text);
		return in.nextLine();
	}
	//inputInteger only
	public int inputInteger(String text) {
		Scanner in=new Scanner(System.in);
		boolean b;
		int s=0;
		do {
			try {
				System.out.print(text);
				s = in.nextInt();
				if(s<=0){
					printError("Value can not negative or 0");
					b=false;
				}else
					b=true;
			} catch (InputMismatchException e) {
				b=false;
				in.nextLine();
				printError("Input Invalid");
			}
		}while (!b);
		return s;
	}

	public float inputFloat(String text) {
		Scanner in=new Scanner(System.in);
		boolean b;
		float s=0;
		do {
			try {
				System.out.print(text);
				s = in.nextFloat();
				if(s<=0){
					printError("Value can not negative or 0");
					b=false;
				}else
					b=true;
			} catch (InputMismatchException e) {
				b=false;
				in.nextLine();
				printError("Input Invalid");
			}
		}while (!b);
		return s;
	}

	//for make redText
	public String redText(String text){
        String ANSI_RED = "\u001B[31m";
		String ANSI_RESET = "\u001B[0m";
		return ANSI_RED+text+ANSI_RESET;
	}
    //Error
	public void printError(String msg){
		System.out.println(redText(msg));
	}

    //Message
	public void msg(String msg){
		Table t=new Table(1, BorderStyle.DESIGN_CASUAL_WIDE);
		t.addCell(redText("----------"+msg+"----------"));
		System.out.println(t.render());
	}
	public void option(){
		OtherComponent component = new OtherComponent();
		int tableNo=1;
		pv.banner(); //show SR G1 running
		do {
			String cmd=pv.menu();
			switch (cmd){
				case "ww":
					add10MProduct();
					break;
				case "*" :
					pv.displayTable(tableNo=1);
					//showProductControllers();
					break;
				case "w" :
					addProductControllers();
					break;
				case "r":
					int id= inputInteger("Input ID to read=> ");
					readControllers(id);
					break;
				case "se":
						component.setRow();
					break;
				case "u":
					updateControllers();
					break;
				case "d":
					deleteControllers();
					break;
				case "s":
					printError("This Function available soon...! Stay tune");
					//searchControllers();
					break;
				case "ba":
                    printError("This Function available soon...! Stay tune");
					break;
				case "h":
					pv.help();
					break;
				case "e":
					System.out.println("You are exit from program..... Good bye!!!");
					System.exit(0);
				case "sa":
					printError("This Function will available soon...! Stay tune");
					break;

				case "g":
					int l =(int) Math.ceil(pd.getProductData().size()/Double.valueOf(component.readRow()));
					tableNo= inputInteger("Go to page: ");
					if(tableNo>l || tableNo<1){
						printError("No page found");
						break;
					}
					pv.displayTable(tableNo);
					break;
				case "l":
					String row = component.readRow();
					tableNo= (int) Math.ceil(pd.getProductData().size()/Double.valueOf(row));
					pv.displayTable(tableNo);
					break;
				case "p":
					tableNo--;
					if (--tableNo<1){
						tableNo = (int) Math.ceil(pd.getProductData().size()/Double.valueOf(component.readRow()));
					}
					pv.displayTable(tableNo);
					break;
				case "n":
					//tableNo++;
					int last =(int) Math.ceil(pd.getProductData().size()/Double.valueOf(component.readRow()));
					if (++tableNo>last) {
						tableNo=1;
					}
					pv.displayTable(tableNo);
					break;
				case "f":
					pv.displayTable(tableNo=1);
					break;
			}

		}while(true);
	}

}
