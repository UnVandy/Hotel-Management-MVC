package Controllers;

import java.io.*;
import java.util.InputMismatchException;
import java.util.Scanner;

public class OtherComponent {
    private ProductControllers pc=new ProductControllers();
    private Scanner in = new Scanner(System.in);

    private void redText(String text){
        String ANSI_RED = "\u001B[31m";
        String ANSI_RESET = "\u001B[0m";

        System.out.println("\n"+ANSI_RED+text+ANSI_RESET);
    }

    private int inputNumber(String text) {
        System.out.print(text);
        int s=5;
        try {
            s=in.nextInt();
        }catch(InputMismatchException e) {
            redText("Input Invalid");
        }
        if(s<=0) {
            redText("You can not set row <=0 ");
            return 5;
        }
        return s;
    }

    public String readRow() {
        String setRow="";
        try {
            BufferedReader br=new BufferedReader(new FileReader("setrow.txt"));
            setRow=br.readLine();
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return setRow;
    }
    void setRow(){
        int set=inputNumber("Please input set row: ");
        try {
            BufferedWriter bw=new BufferedWriter(new FileWriter("setrow.txt"));
            bw.write(set+"");
            pc.msg("Set row was sucessfully update...");
            bw.close();
        }catch(IOException e) {
            e.printStackTrace();
        }

    }
    public static String blueText(int text){
        String ANSI_BLUE = "\u001B[34m";
        String ANSI_RESET = "\u001B[0m";
        return ANSI_BLUE+text+ANSI_RESET;
    }
    public static String sign(String text){
        String ANSI_RED = "\u001B[31m";
        String ANSI_RESET = "\u001B[0m";
        return ANSI_RED+text+ANSI_RESET;
    }
}
