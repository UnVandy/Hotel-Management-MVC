package Models.DTO;

public class Product {
    private int id;
    private String name;
    private int qty;
    private float price;
    private String date;

    public Product(){
        this.id=0;
        this.name="";
        this.price=0;
        this.qty=0;
        this.date="";
    }
    public Product(String name,int qty,float price){
        this.name=name;
        this.qty=qty;
        this.price=price;
    }
    public Product(int id, String name, int qty, float price, String date) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.qty = qty;
        this.date = date;
    }
    public Product(  String name ) {
        this.name = name;
    }
    public Product(int id, String name, int qty, float price) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.qty = qty;
    }
    public Product(int id, String name) {
        this.id = id;
        this.name = name;
    }
    public Product(int id, float price) {
        this.id = id;
        this.price = price;
    }
    public Product(int id,int qty){
        this.qty=qty;
        this.id=id;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    @Override
    public String toString() {
        return "ProductData{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", qty=" + qty +
                ", price=" + price +
                ", date='" + date + '\'' +
                '}';
    }

}
