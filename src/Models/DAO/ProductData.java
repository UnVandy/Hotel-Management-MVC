package Models.DAO;
import Controllers.ProductControllers;
import Models.DTO.Product;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

// Only create method read and write here
public class ProductData {
    private PreparedStatement ps;
    private ConnectDB connect=new ConnectDB();
    private Statement st;
    private ResultSet rs;

    //Add Product
    public void addProduct(Product pro){
        connect.openConnection();
        String sql="INSERT INTO tbproduct (id,name,qty,price) VALUES (?,?,?,?)";
        try {
            ps =connect.con.prepareStatement(sql);
            ps.setInt(1,pro.getId());
            ps.setString(2,pro.getName());
            ps.setInt(3,pro.getQty());
            ps.setFloat(4,pro.getPrice());
            ps.executeUpdate();
            connect.con.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        connect.close();
    }

    //Select Product
    public List<Product> getProductData(){
        List<Product> list=new ArrayList<>();
        String sql="SELECT * FROM tbProduct ORDER BY id";
        connect.openConnection();
        try {
            st=connect.con.createStatement();
            rs=st.executeQuery(sql);
            while (rs.next()){
                Product pro = new Product(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getFloat(4), rs.getString(5));
                list.add(pro);
            }
            connect.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    //Read
    public Product read(int id){
        ProductControllers pc=new ProductControllers();
        Product pro=new Product();
        String sql="SELECT * FROM tbProduct WHERE id='"+id+"'";
        connect.openConnection();
        try{
            st=connect.con.createStatement();
            rs=st.executeQuery(sql);
            if(rs.next()){
                pro=new Product(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getFloat(4),rs.getString(5));
            }
        }catch (SQLException e){
            pc.msg("Connection Error");
        }
        connect.close();
        return pro;
    }
    public int readConditon(int id){
        int i=0;
        ProductControllers pc=new ProductControllers();
        Product pro=new Product();
        String sql="SELECT * FROM tbProduct WHERE id='"+id+"'";
        connect.openConnection();
        try{
            st=connect.con.createStatement();
            rs=st.executeQuery(sql);
            if(rs.next()){
                i=rs.getInt(1);
            }else {
            }
        }catch (SQLException e){
            pc.msg("Connection Error");
        }
        connect.close();
        return i;
    }
    //Update all
    public void update(Product pro){
        ProductControllers pc=new ProductControllers();
        String sql="UPDATE tbProduct SET name=?,qty=?,price=? WHERE id=?";
        connect.openConnection();
        try{
            ps=connect.con.prepareStatement(sql);
            ps.setString(1,pro.getName());
            ps.setInt(2,pro.getQty());
            ps.setFloat(3,pro.getPrice());
            ps.setInt(4,pro.getId());
            ps.executeUpdate();
            pc.msg("Update successfully");
        }catch (SQLException e){
            pc.msg("Update Error");
        }
        connect.close();
    }
    public void updateName(Product pro){
        ProductControllers pc=new ProductControllers();
        String sql="UPDATE tbProduct SET name=? WHERE id=?";
        connect.openConnection();
        try{
            ps=connect.con.prepareStatement(sql);
            ps.setString(1,pro.getName());
            ps.setInt(2,pro.getId());
            ps.executeUpdate();
            pc.msg("Update successfully");
        }catch (SQLException e){
            pc.msg("Update Error");
        }
        connect.close();
    }
    public void updateQty(Product pro){
        ProductControllers pc=new ProductControllers();
        String sql="UPDATE tbProduct SET qty=? WHERE id=?";
        connect.openConnection();
        try{
            ps=connect.con.prepareStatement(sql);
            ps.setInt(1,pro.getQty());
            ps.setInt(2,pro.getId());
            ps.executeUpdate();
            pc.msg("Update successfully");
        }catch (SQLException e){
            pc.msg("Update Error");
        }
        connect.close();
    }
    public void updatePrice(Product pro){
        ProductControllers pc=new ProductControllers();
        String sql="UPDATE tbProduct SET price=? WHERE id=?";
        connect.openConnection();
        try{
            ps=connect.con.prepareStatement(sql);
            ps.setFloat(1,pro.getPrice());
            ps.setInt(2,pro.getId());
            ps.executeUpdate();
            pc.msg("Update successfully");
        }catch (SQLException e){
            pc.msg("Update Error");
        }
        connect.close();
    }

    //Delete
    public void delete(int id){
        ProductControllers pc=new ProductControllers();
        String sql="Delete from tbProduct where id=?";
        connect.openConnection();
        try{
            ps=connect.con.prepareStatement(sql);
            ps.setInt(1,id);
            ps.executeUpdate();
        }catch(SQLException e){
            pc.printError("Delete Error");
        }
        connect.close();
    }

    //Search
    public List<Product> search(String id){
        ProductControllers pc=new ProductControllers();
        List<Product> list=new ArrayList<>();
        Product pro;
        String sql="SELECT *from tbproduct WHERE name LIKE'%"+id+"%'";
        connect.openConnection();
        try{
            st=connect.con.createStatement();
            rs=st.executeQuery(sql);
            while(rs.next()){
                pro=new Product(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getFloat(4),rs.getString(5));
                list.add(pro);
            }
        }catch (SQLException e){
            pc.msg("Connection Error");
        }
        connect.close();
        return list;
    }

    //getLastID
    public int getLastID(){
        ProductControllers pc=new ProductControllers();
        int id=0;
        String sql="SELECT MAX(id) FROM tbProduct";
        connect.openConnection();
        try{
            st=connect.con.createStatement();
            rs=st.executeQuery(sql);
            if(rs.next()){
                id=rs.getInt(1);
            }
            connect.close();
        }catch(SQLException e){
            pc.msg("Error get Max ID in method getLastID");
        }
        return id;
    }



}
