package Models.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectDB {
    public Connection con;

    public void openConnection(){
        String url="jdbc:postgresql://localhost:5432/Product";
        try{
            Class.forName("org.postgresql.Driver");
            con= DriverManager.getConnection(url,"postgres","679798");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
    public void close(){
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
